<html>
<head><title>Dada File Searcher: Frequency Count</title></head>
<body>

<h2>DADA File Searcher: Frequency Count for <?php echo $_POST["root"]; ?></h2>

Interpreting the table: 
<p>
When the correlator dumps data at a certain time, it dumps many DADA files, representing the data recorded at different frequencies, but at the same time. All of these files will have the same name, such as 2014-07-07-14:51:29_0000360537538560.000000.dada, but will be stored in different directories, such as ledavoro1, ledovro2. The Frequency count is how many files of the same name, with different frequencies,  are stored. Normally this should be 22. It is not enough, however, that there are 22 files - the files must also be valid. The current definition of valid is: They must contain data, and have the same number of correlator dumps in them, and no incomplete dumps.
<p>
In the table, the first column is a file name. Files with this name will appear in several different directories. In those directories
the file may have zipped and unzipped versions, so the file extension is not given here. The next column indicates "Yes" if all frequencies are present for this file name,
and the files are all valid. The next column "UNKNOWN" indicates filenames that have files for which nothing can be determined - they are empty or corrupt. The next columns represent all the frequencies generated from the correlator, with the frequency in MHz as the column header. The value in frequency column is the number of correlator dumps in the file for that frequency. The numbers should be the same across the row.
<p>
Rows are in reverse date/time order.
<p>
<?php

function snumber($num) {
  if ( $num == "UNKNOWN" ) return $num;
  else if ( intval($num)-$num == 0 ) return intval($num);
  else return $num;
}

function is_in($val, $a) {
  foreach ( $a as $el ) 
    if ( $el == $val ) return True;
  return False;
}

function get_scans($f, $list) {
  foreach ( $list as $l ) {
    $freq_scan_fields = explode(";",$l);
    $scans = $freq_scan_fields[1];
    if ( $freq_scan_fields[0] == $f ) return $scans;
  }
  return "";
}

function reason_is_bad($badness) {
  if ( $badness == "BAD_1" ) return "Number of scans aren't all the same";
  else if ( $badness == "BAD_2" ) return "Number of scans is not positive integer";
  else if ( $badness == "BAD_3" ) return "There are not 22 frequencies present";
  else return "Unknown reason";
}

if ( empty($_POST["root"]) ) {
  echo "No ROOT!</body></html>";
  exit(0);
}

$database = fopen("freq_count.txt","r");
while ( $line = fgets($database,512) ) {
  $fields = explode(",",$line);

  if ( $fields[0] == $_POST["root"] ) {
    $freq_scans = explode(":",$fields[2]);
    foreach ( $freq_scans as $fs ) {
      $freq_scan_fields = explode(";",$fs);
      if ( !empty($freq_scan_fields[0]) ) $freqs[] = $freq_scan_fields[0]; 
    }
  }
}
fclose($database);
$freqs = array_unique($freqs);
sort($freqs,SORT_NUMERIC);
   
# Count. Same loop as later.
$num_good = 0;
$num_bad = 0;
$database = fopen("freq_count.txt","r");
while ( $line = fgets($database,512) ) {
  $fields = explode(",",$line);


  if ( $fields[0] == $_POST["root"] ) {
    $freq_scans = explode(":",$fields[2]);
    foreach ( $freq_scans as $fs ) {
      $freq_scan_fields = explode(";",$fs);
      if ( !empty($freq_scan_fields[0]) ) $line_freqs[] = $freq_scan_fields[0];
    }

    if ( trim($fields[4]) == "GOOD" ) ++$num_good; else ++$num_bad;
  }
}
fclose($database);

echo "Number of good sets: ".strval($num_good)." Number of bad sets: ".strval($num_bad);

echo "<table border cellpadding=3>";
echo "<tr><th>File</th><th>Ok</th>";
foreach ( $freqs as $freq ) {
  echo "<th>".$freq."</th>";
}
echo "</tr>";

$database = fopen("freq_count.txt","r");
while ( $line = fgets($database,512) ) {
  $fields = explode(",",$line);


  if ( $fields[0] == $_POST["root"] ) {
    $line_freqs = array();
    $freq_scans = explode(":",$fields[2]);
    foreach ( $freq_scans as $fs ) {
      $freq_scan_fields = explode(";",$fs);
      if ( !empty($freq_scan_fields[0]) ) $line_freqs[] = $freq_scan_fields[0];
    }
    $line_freqs = array_unique($line_freqs);

    echo "<tr><td>".$fields[1]."</td><td>";
    if ( trim($fields[4]) == "GOOD" ) echo "Yes.</td>"; else echo "No. ".reason_is_bad(trim($fields[4]))."</td>";
    foreach ( $freqs as $freq ) {
      if ( is_in($freq, $line_freqs) ) {
       echo "<td>"; echo snumber(get_scans($freq, $freq_scans)); echo "</td>";
      } else echo "<td>-</td>";
    }
    echo "</tr>";
  }
}
fclose($database);

echo "</table>";
?>

</body><html>
