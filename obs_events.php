<html>
<head><title>Dada File Searcher: Frequency Count</title></head>
<body>

<?php
if ( $_GET["into"] == "continu" ) $_POST["root"] = "/nfs/data";
if ( empty($_POST["root"]) ) {
  echo "No ROOT!</body></html>";
  exit(0);
}
?>

<h2>Observing Dates/Times for <?php echo $_POST["root"]; ?></h2>
Increase your browser width so that lines are not broken. What follows is a list of days, with each 
day indicated by its date e.g. 2015-08-17. Inside each day is a timeline, going from 0 hours to 23.59 hours.
The times when an observation was made are  indicated by  red bars. The days are listed in reverse time order, i.e.
most recent first.
<p>

<?php
function is_in($list, $val) {
  foreach ($list as $v) {
    if ( $v == $val ) return True;
  }
  return False;
}

function convert_to_min($time) {
  return (intval($time[0])*10+intval($time[1]))*60+intval($time[2])*10+intval($time[3]);
}

function times_for_day($date, $date_times) {
  // Divide a day into 10 minute slots
  $histogram_of_times = array_fill(0, 24*6, "-");

  foreach ( $date_times as $date_time ) {
    $fields = explode(":",$date_time);
    if ( $date == $fields[0] ) {
      $histogram_of_times[convert_to_min($fields[1])/10] = "<b><font color=\"red\">|</font></b>";
    }
  }
  return $histogram_of_times;
}

$time_names = array_fill(0, 24*6, "-");
$time_names[0] = "0"; $time_names[6] = "1"; $time_names[12] = "2"; $time_names[18] = "3"; $time_names[24] = "4";
$time_names[30] = "5"; $time_names[36] = "6"; $time_names[42] = "7"; $time_names[48] = "8"; $time_names[54] = "9";
$time_names[60] = "1"; $time_names[61] = "0"; $time_names[66] = "1"; $time_names[67] = "1"; $time_names[72] = "1"; $time_names[73] = "2";
$time_names[78] = "1"; $time_names[79] = "3"; $time_names[84] = "1"; $time_names[85] = "4";
$time_names[90] = "1"; $time_names[91] = "5"; $time_names[96] = "1"; $time_names[96] = "6";
$time_names[102] = "1"; $time_names[103] = "7"; $time_names[108] = "1"; $time_names[109] = "8";
$time_names[114] = "1"; $time_names[115] = "9"; $time_names[120] = "2"; $time_names[121] = "0";
$time_names[126] = "2"; $time_names[127] = "1"; $time_names[132] = "2"; $time_names[133] = "2";
$time_names[138] = "2"; $time_names[139] = "3";

$database = fopen("catalog.txt","r");
while ( $line = fgets($database,512) ) {
  $fields = explode(",",$line);
  if ( substr($fields[0],0,strlen($_POST["root"])) == $_POST["root"] and $fields[1] != "UNKNOWN" ) {
    $dates[] = $fields[1];
    $date_times[] = $fields[1].":".$fields[2];
  }
}
fclose($database);

$dates = array_unique($dates);
rsort($dates);

foreach  ( $dates as $date ) {
  echo "<h3>".$date[0].$date[1].$date[2].$date[3]."-".$date[4].$date[5]."-".$date[6].$date[7]."</h3>";
  echo "<font face=\"courier\">";
  echo implode($time_names);
  echo "<br>";
  echo implode(times_for_day($date, $date_times));
  echo "</font>"; 
}
?>

</body></html>

