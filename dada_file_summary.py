import zlib
import numpy as np
import os, sys
import make_header
from sets import Set

HEADER_SIZE = 4096

def dada_format(filename):
  f = open(filename, 'rb')
  headerstr = f.read(HEADER_SIZE)
  f.close()

  for line in headerstr.split('\n'):
    l = line.split()
    if len(l) >= 2 and l[0] == "DATA_ORDER": return l[1]
  return ""  

def is_positive_integer(s):
  x = float(s)
  return x > 0 and int(x) == x

def only_positive_integers(values):
  for v in values:
    if not is_positive_integer(v): return False
  return True

def one_value(simple_set):
  for s in simple_set:
    if len(s) > 0: return s
  return "UNKNOWN"


def size_uncompressed(infilename):
	"""
	Count the number of bytes in a compressed file when it is uncompressed, thus getting its true size
	"""

	print "Unzipping", infilename
	# From Ben's dadazip code.
	nblock = 4

	count = 0
	infilesize = os.path.getsize(infilename)
	if infilesize <= 4096: return infilesize 

 	infilesize -= HEADER_SIZE
	decompress = zlib.decompressobj()
	with open(infilename, 'rb') as fin:
		
		header = fin.read(HEADER_SIZE)
		count += HEADER_SIZE

		max_block_size = (infilesize-1) // nblock + 1
		if max_block_size <= 0: return 
		for offset in xrange(0, infilesize, max_block_size):
			block_size = min(max_block_size, infilesize-offset)
			data = fin.read(block_size)
			insize = len(data)
			data = decompress.decompress(data)
			count += len(data)

		count += len(decompress.flush())

	return count

def is_dada_zipped(name):
  return name.find("201") == 0 and name[-8:] == ".dadazip"

def is_dada(name):
  return name.find("201") == 0 and name[-5:] == ".dada"

def get_file_size(filename, zip_file_list):
  """
	There are 3 chances to get the file size.
	1. It's not a zipped file - just "ls"
	2. It's zipped and it's in the file_size list - read the file list.
	3. It's zipped and it'll have to be unzipped to get the true size.
  """

  if is_dada_zipped(filename): 
    if zip_file_list: 
      for line in open(zip_file_list,"r"):
        info = line.split(",")
        if info[0] == filename: return info[1][:-1]

    return size_uncompressed(filename)
  else: return os.path.getsize(filename)


def format_n_scans(n):
  if n == "UNKNOWN": return n
  else: return ( "%.6f" % n)

def process_directory(ROOT, file_sizes=None):
  new_catalog = open("catalog.txt", "w")
  for ovro in range(1,12):
    for sub_dir in [ "data1", "data2" ]:
      for num in [ "one", "two" ]: 
        dir_name = ROOT+"/ledaovro"+str(ovro)+"/"+sub_dir+"/"+num
        print "Process",dir_name
        if os.path.isdir(dir_name):
          for filename in os.listdir(dir_name):

	    process_file_name = ""
	    if is_dada_zipped(filename):	
	      # See if there is an equivalent DADA. Ignore this one if there is.
	      dada_name = filename[:-3]      # remove "zip"
	      if not os.path.isfile(dada_name):
	        process_file_name = dir_name+"/"+filename
            elif is_dada(filename): process_file_name = dir_name+"/"+filename

            if process_file_name != "" and dada_format(process_file_name) in [ "REG_TILE_TRIANGULAR_2x2", "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX" ] :
	 
	      if is_dada_zipped(filename): 
	        size = get_file_size(process_file_name, file_sizes)
                info = make_header.make_header(process_file_name, False, False, size)
	        new_catalog.write(dir_name+"/"+filename+","+info["DATE"]+","+info["TIME"]+","+info["LST"]+","+info["DATA_ORDER"]+","+str(info["FREQCENT"])+","+str(size)+","+format_n_scans(info["N_SCANS"])+"\n")
	      else:
	        info = make_header.make_header(process_file_name, False, False)
	        new_catalog.write(dir_name+"/"+filename+","+info["DATE"]+","+info["TIME"]+","+info["LST"]+","+info["DATA_ORDER"]+","+str(info["FREQCENT"])+","+str(info["FILE_SIZE"])+","+format_n_scans(info["N_SCANS"])+"\n")
  new_catalog.close()
 
  # Different file - frequency count
  print "Calculating freq count"

  info = []	# List of records, one for each time stamp of a file
  for line in open("catalog.txt", "rU"):
    l = line[:-1].split(",")
    path = l[0].split("/")
    filename = path[6]
    if is_dada(filename): filename = filename[:-5]	# strip extension
    else: filename = filename[:-8]
    
    frequency = l[5]
    n_scans = l[7]
    
    # find the file in info
    where = -1
    for i in range(len(info)):
      if info[i][0] == filename: 
        where = i
        break
    
    # Insert information about the file in info
    if where >= 0:
      info[where][1].add(str(frequency)+";"+str(n_scans))
      info[where][2].add(n_scans)
    else:
      # Generate a new record which goes on the list
      new_info = ( filename, Set([str(frequency)+";"+str(n_scans)]), Set([n_scans]), Set([]) )    # Create the set from a list
      info.append(new_info)

  # File format: Top level in a line is comma separated. ROOT directory,filename (no ext),How many scans,Unique scans,Code
  # How many scans is colon separated. Each entry is the number of scans for a frequency, of the form <freq>;<n scans>
  # Unique scans is colon separated, it is a list of #scans present
  # Code is GOOD,BAD_{1,2,3}
  cross_count = open("tmp_freq_count.txt","w")
  for index, inf in enumerate(info):
    fline = ROOT+","
    fline += inf[0]+","
    for x in inf[1]: fline += x+":"
    fline = fline[:-1]
    fline += ","
    for x in inf[2]: fline += x+":"
    fline = fline[:-1]
    if not only_positive_integers(inf[2]): 
      fline += ",BAD_2"	     # Number of scans is not integer
      info[index][3].add("BAD_2")
    elif len(inf[2]) != 1: 
      fline += ",BAD_1"	# Number of scans aren't all the same
      info[index][3].add("BAD_1")
    elif len(inf[1]) != 22: 
      fline += ",BAD_3"		# There are not 22 frequencies present
      info[index][3].add("BAD_3")
    else: 
      fline += ",GOOD"
      info[index][3].add("GOOD")
 
    cross_count.write(fline+"\n")

  print "Adding frequency code"
  # Do a partial join of the files. Want the frequency code in catalog too.
  new_catalog= open("x.txt","w")
  for line in open("catalog.txt","rU"):
    path = line[:-1].split(",")[0]
    short_name = path.split("/")[-1]
    if is_dada(short_name): short_name = short_name[:-5]	# strip extension
    else: short_name = short_name[:-8]

    # Find the frequency code
    for inf in info:
      if short_name == inf[0]:
        if len(inf[3]) == 0: print line
        new_catalog.write(line[:-1]+","+one_value(inf[3])+"\n")
  new_catalog.close()
  os.system("sort -t , -k 2,3  -r x.txt > catalog.txt")    # Reverse date/time order
  os.system("sort -t , -k1 -r tmp_freq_count.txt > freq_count.txt")
        
try: os.remove("all_catalog.txt")
except: pass
try: os.remove("all_freq_count.txt")
except: pass
for nfs_dir in os.listdir("/nfs"):
  ROOT = "/nfs/"+nfs_dir
  if os.path.isdir(ROOT):
    process_directory(ROOT)
    # Catenate this catalog odnto the global one
    os.system("cat catalog.txt >> all_catalog.txt")
    os.system("cat freq_count.txt >> all_freq_count.txt")
